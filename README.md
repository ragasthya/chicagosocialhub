# ChicagoSocialHub
## Authors: Rahul S Agasthya, Sobish Mahajan

This is a Project part of the CSP: 586 (Software Modelling Development with UML) course. 

### Responsibility

Search for places on a street, and show divvy nearest dock stations for a selected place. And a Dashboard to show the different review counts and ratings using pie-chart, bar-char, stacked-chart, etc. for the search results for top rated or reviewed places based on a filter specified by the user. 

### Deadlines

**Phase I**
*Project feature list, requirements, use cases and use-case diagram.*

March 9th, 2019

**Phase II** 
*Project development including domain model, design model, Sequence diagrams, and design patterns utilized, 5-minutes video recording of the project implementation and run.*

April 27th, 2019

**Phase I**
*Final Delivery.*

May 4th, 2019

### Software Used
* Angular
* Node.js/Express
* PostgreSQL – To store Divvy station status
* ElasticSearch – To store Yelp reviews for Chicago Businesses
* Python

### Node Modules to be installed:
1. ```npm install express```
2. ```npm install moment  --save```
3. ```npm install elasticsearch```
4. ```npm install d3```

### Steps to successfully run the project:
1. On command line, navigate to the elasticsearch-7.0.0/bin
2. Run elastic search by executing the following command:
	```elasticsearch```
3. Move logstash-6.6.2 folder to the C drive.
4. On another command line window, navigate the logstash-6.6.2/logstash-6.6.2/bin. (This must be the same folder moved to C drive)
5. Run logstash by executing the following command:
	```logstash -f logstash.conf```
6. On the jupyter notebook, open backend-build-yelp-reviews/ChicagoSocialHub-Yelp.ipynb and run this file by clicking on Cell > Run All
7. On another command line window, navigate to the backend folder and run the following command to start the backend:
	```node server.js```
8.  To fetch the data from logstash to elastic search, on another command line window, navigate to backend-build-divvy-status and execute the following command:
	```python divvy_stations_status_logs.py```
9. Either on Visual Studio or on another command line navigate to the frontend folder and execute the following command:
	```npm start```
