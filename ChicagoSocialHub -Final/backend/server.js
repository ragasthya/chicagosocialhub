////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////


/// This file and the source code provided can be used only for   
/// the projects and assignments of this course

/// Last Edit by Dr. Atef Bader: 1/27/2019


////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
//////////////////////              SETUP NEEDED                ////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

//  Install Nodejs (the bundle includes the npm) from the following website:
//      https://nodejs.org/en/download/


//  Before you start nodejs make sure you install from the  
//  command line window/terminal the following packages:
//      1. npm install express
//      2. npm install pg
//      3. npm install pg-format
//      4. npm install moment --save
//      5. npm install elasticsearch


//  Read the docs for the following packages:
//      1. https://node-postgres.com/
//      2.  result API: 
//              https://node-postgres.com/api/result
//      3. Nearest Neighbor Search
//              https://postgis.net/workshops/postgis-intro/knn.html    
//      4. https://www.elastic.co/guide/en/elasticsearch/client/javascript-api/current/quick-start.html
//      5. https://momentjs.com/
//      6. http://momentjs.com/docs/#/displaying/format/


////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////


const express = require('express');

var pg = require('pg');

var bodyParser = require('body-parser');

const moment = require('moment');


// Connect to elasticsearch Server

const elasticsearch = require('elasticsearch');
const esClient = new elasticsearch.Client({
  host: '127.0.0.1:9200',
  log: 'error'
});


// Connect to PostgreSQL server

var conString = "pg://postgres:root@127.0.0.1:5432/chicago_divvy_stations";
var pgClient = new pg.Client(conString);
pgClient.connect();

var find_places_task_completed = false;         
var find_stations_task_completed= false;    


const app = express();
const router = express.Router();


app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

router.all('*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});



var places_found = [];
var stations_found = [];
var stations_found_docks=[];
var stations_found_docks_hourly=[];
var place_selected;
var station_selected;
var save_id_station;


/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

//////   The following are the routes received from NG/Browser client        ////////

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////



router.route('/places').get((req, res) => {

    res.json(places_found)
    
});


router.route('/place_selected').get((req, res) => {

    res.json(place_selected)
   
});



router.route('/allPlaces').get((req, res) => {

    res.json(places_found)
   
});




router.route('/stations').get((req, res) => {
   
    res.json(stations_found)
           
});

router.route('/stations/docks').get((req, res) => {
   
    res.json(stations_found_docks)
           
});

router.route('/stations/docks/all').get((req, res) => {
   
    res.json(stations_found_docks)
           
});



router.route('/stations/docks/hourly').get((req, res) => {
   
    res.json(stations_found_docks_hourly)
           
});



router.route('/stations/docks/hourly').post((req, res) => {
   
    var str = JSON.stringify(req.body, null, 4);

    station_selected=save_id_station;

    const query3 = {
        name: 'fetch-divvy-docks-hourly',
        text: 'SELECT  * FROM divvy_stations_status_log where id= $1 ORDER BY lastCommunicationTime DESC LIMIT 720 ',
        values: [station_selected]
    }

    find_stations_from_divvy_docks_hourly(query3).then(function (response) {
        var hits = response;
        res.json(stations_found_docks_hourly);
    });


    //console.log("station_found_server Divvy 3--->"+req.body.id);

    //res.json(stations_found_docks) 
});




router.route('/stations/docks').post((req, res) => {
   
    var str = JSON.stringify(req.body, null, 4);

    find_stations_task_completed=false;


    find_stations_from_divvy_docks(req.body.id).then(function (response) {
        var hits = response;
        res.json(stations_found_docks);
        //console.log(stations_found_docks);
        //res.json({'stations_found_docks': 'Added successfully'});
    });

});




router.route('/stations/docks/all').post((req, res) => {
   
    var str = JSON.stringify(req.body, null, 4);

    find_stations_task_completed=false;


    find_stations_from_divvy_docks_all().then(function (response) {
        var hits = response;
        res.json(stations_found_docks);

        //res.json({'stations_found_docks': 'Added successfully'});
    });

});



router.route('/places/find').post((req, res) => {

    var str = JSON.stringify(req.body, null, 4);

    find_places_task_completed = false;             

    find_places_from_yelp(req.body.find, req.body.where).then(function (response) {
        var hits = response;
        res.json(places_found);
    });

});


router.route('/places/find/zipcode').post((req, res) => {

    var str = JSON.stringify(req.body, null, 4);

    find_places_task_completed = false;         
    

    find_places_from_yelp_zipcode(req.body.find, req.body.where, req.body.zipcode).then(function (response) {
        var hits = response;
        res.json(places_found);
    });

});

router.route('/places/zipcode').post((req, res) => {

    var str = JSON.stringify(req.body, null, 4);

    find_places_task_completed = false;         
    

    find_places_from_yelp_zipcode_only(req.body.zipcode).then(function (response) {
        var hits = response;
        res.json(places_found);
    });

});


/* router.route('/stations/docks').post((req, res) => {
   
    var str = JSON.stringify(req.body, null, 4);

    find_stations_task_completed=false;

    console.log("station_found_server Divvy"+req.body.id);

    find_stations_from_divvy_docks(req.body.id).then(function (response) {
        var hits = response;
        res.json(stations_found_docks);
        //console.log(stations_found_docks);
        //res.json({'stations_found_docks': 'Added successfully'});
    });

});
 */



router.route('/stations/find').post((req, res) => {

    var str = JSON.stringify(req.body, null, 4);

    for (var i = 0,len = places_found.length; i < len; i++) {

        if ( places_found[i].name === req.body.placeName ) { // strict equality test

            place_selected = places_found[i];

            break;
        }
    }
 
    const query = {
        // give the query a unique name
        name: 'fetch-divvy',
        text: ' SELECT * FROM divvy_stations_status ORDER BY (divvy_stations_status.where_is <-> ST_POINT($1,$2)) LIMIT 3',
        values: [place_selected.latitude, place_selected.longitude]
    }

    find_stations_from_divvy(query).then(function (response) {
        var hits = response;
        res.json({'stations_found': 'Added successfully'});
    });
 

});



/* router.route('/stations/find').post((req, res) => {

    var str = JSON.stringify(req.body, null, 4);

    for (var i = 0,len = places_found.length; i < len; i++) {

        if ( places_found[i].name === req.body.placeName ) { // strict equality test

            place_selected = places_found[i];

            break;
        }
    }
    */
    










/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

////////////////////    Divvy - PostgreSQL - Client API            /////////////////

////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

async function find_stations_from_divvy(query) {

    const response = await pgClient.query(query);

    stations_found = [];

    for (i = 0; i < 3; i++) {
                
        dock_id = response.rows[i].id;
        
        let body = {
            size: 1000,
            from: 0,
            "query": {
              "bool" : {
                "must" : {
                   "term" : { "id" : dock_id } 
                }            
              }
            }
        }
        
        stations_found_docks = [];

        results = await esClient.search({index: 'divvy_stations_logs', body: body});

      //  console.log('Results -->'+results);
        results.hits.hits.forEach((hit, index) => {
    
            percent_filled = hit._source.availableBikes * 100 / hit._source.totalDocks;

            percent_filled = percent_filled.toFixed(2);

            var station = {
                "id": hit._source.id,
                "stationName": hit._source.stationName,
                "availableBikes": hit._source.availableBikes,
                "availableDocks": hit._source.availableDocks,
                "is_renting": hit._source.is_renting,
                "lastCommunicationTime": hit._source.lastCommunicationTime,
                "latitude": hit._source.latitude,
                "longitude": hit._source.longitude,
                "status": hit._source.status,
                "totalDocks": hit._source.totalDocks,
                "percent_filled": percent_filled
            };

            stations_found_docks.push(station);

        });

      //  console.log(stations_found_docks);

        stations_found_docks = stations_found_docks.sort(function compare(a, b) {
        var x = Date.parse(a.lastCommunicationTime);
        var y = Date.parse(b.lastCommunicationTime);
        return x - y;
    });
    stations_found_docks = stations_found_docks.reverse();

    console.log(stations_found_docks);

    stations_found.push(stations_found_docks[0]);

    }


}

async function find_stations_from_divvy_docks_all(id) {

    stations_found_docks = [];

    let body = {
        size: 1000,
        from: 0,
        "query": {
          "bool" : {
          }
        }
    }


    results = await esClient.search({index: 'divvy_stations_logs', body: body});

    results.hits.hits.forEach((hit, index) => {
        

        var station = {
            "id": hit._source.id,
            "stationName": hit._source.stationName,
            "availableBikes": hit._source.availableBikes,
            "availableDocks": hit._source.availableDocks,
            "is_renting": hit._source.is_renting,
            "lastCommunicationTime": hit._source.lastCommunicationTime,
            "latitude": hit._source.latitude,
            "longitude": hit._source.longitude,
            "status": hit._source.status,
            "totalDocks": hit._source.totalDocks
        };

        stations_found_docks.push(station);

    });

    /* stations_found_docks = stations_found_docks.sort(function compare(a, b) {
        var x = Date.parse(a.lastCommunicationTime);
        var y = Date.parse(b.lastCommunicationTime);
        return x - y;
   */ // });
    //stations_found_docks = stations_found_docks.reverse();

    find_stations_task_completed = true;     
    
    

}


async function find_stations_from_divvy_docks(id) {

    stations_found_docks = [];

    let body = {
        size: 1000,
        from: 0,
        "query": {
          "bool" : {
            "must" : {
               "term" : { "id" : id } 
            }            
          }
        }
    }


    results = await esClient.search({index: 'divvy_stations_logs', body: body});

    results.hits.hits.forEach((hit, index) => {
        var station = {
            "id": hit._source.id,
            "stationName": hit._source.stationName,
            "availableBikes": hit._source.availableBikes,
            "availableDocks": hit._source.availableDocks,
            "is_renting": hit._source.is_renting,
            "lastCommunicationTime": hit._source.lastCommunicationTime,
            "latitude": hit._source.latitude,
            "longitude": hit._source.longitude,
            "status": hit._source.status,
            "totalDocks": hit._source.totalDocks
        };

        stations_found_docks.push(station);

    });

        stations_found_docks = stations_found_docks.sort(function compare(a, b) {
        var x = Date.parse(a.lastCommunicationTime);
        var y = Date.parse(b.lastCommunicationTime);
        return x - y;
    });
    stations_found_docks = stations_found_docks.reverse();

    find_stations_task_completed = true;     
    
    

}

async function find_stations_from_divvy_docks_hourly(query3) {

    const response = await pgClient.query(query3);

    stations_found_docks_hourly = [];

    for (i = 0; i < response.rows.length; i++) {
                
        plainTextDateTime2 =  moment(response.rows[i].lastcommunicationtime).format('YYYY-MM-DD, h:mm:ss a');

    //    plainTextDateTime =  moment(response.rows[i].lastcommunicationtime).format('YYYY-MM-DD, h:mm:ss a');

    var place = {
        "name": hit._source.name,
        "display_phone": hit._source.display_phone,
        "address1": hit._source.location.address1,
        "is_closed": hit._source.is_closed,
        "rating": hit._source.rating,
        "review_count": hit._source.review_count,
        "latitude": hit._source.coordinates.latitude,    
        "longitude": hit._source.coordinates.longitude
};

        stations_found_docks_hourly.push(station_docks);

    }


}



/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

////////////////////    Yelp - ElasticSerch - Client API            /////////////////

////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////



async function find_places_from_yelp(place, where) {

    places_found = [];

//////////////////////////////////////////////////////////////////////////////////////
// Using the business name to search for businesses will leead to incomplete results
// better to search using categorisa/alias and title associated with the business name
// For example one of the famous places in chicago for HotDogs is Portillos
// However, it also offers Salad and burgers
// Here is an example of a busness review from Yelp for Pertilos
//               alias': 'portillos-hot-dogs-chicago-4',
//              'categories': [{'alias': 'hotdog', 'title': 'Hot Dogs'},
//                             {'alias': 'salad', 'title': 'Salad'},
//                             {'alias': 'burgers', 'title': 'Burgers'}],
//              'name': "Portillo's Hot Dogs",
//////////////////////////////////////////////////////////////////////////////////////


    let body = {
        size: 1000,
        from: 0,
        "query": {
          "bool" : {
            "must" : {
               "term" : { "categories.alias" : place } 
            },


            "filter": {
                "term" : { "location.address1" : where  }
            },


            "must_not" : {
              "range" : {
                "rating" : { "lte" : 3 }
              }
            },

            "must_not" : {
              "range" : {
                "review_count" : { "lte" : 500 }
              }
            },

            "should" : [
              { "term" : { "is_closed" : "false" } }
            ],
          }
        }
    }


    results = await esClient.search({index: 'chicago_yelp_reviews', body: body});

    results.hits.hits.forEach((hit, index) => {
        

        var place = {
            "name": hit._source.name,
            "display_phone": hit._source.display_phone,
            "address1": hit._source.location.address1,
            "zip_code": hit._source.location.zip_code,
            "is_closed": hit._source.is_closed,
            "rating": hit._source.rating,
            "review_count": hit._source.review_count,
            "latitude": hit._source.coordinates.latitude,    
            "longitude": hit._source.coordinates.longitude
    };

        places_found.push(place);

    });

    find_places_task_completed = true;     
    
}



async function find_places_from_yelp_zipcode(place, where, zipcode) {

    places_found = [];

//////////////////////////////////////////////////////////////////////////////////////
// Using the business name to search for businesses will leead to incomplete results
// better to search using categorisa/alias and title associated with the business name
// For example one of the famous places in chicago for HotDogs is Portillos
// However, it also offers Salad and burgers
// Here is an example of a busness review from Yelp for Pertilos
//               alias': 'portillos-hot-dogs-chicago-4',
//              'categories': [{'alias': 'hotdog', 'title': 'Hot Dogs'},
//                             {'alias': 'salad', 'title': 'Salad'},
//                             {'alias': 'burgers', 'title': 'Burgers'}],
//              'name': "Portillo's Hot Dogs",
//////////////////////////////////////////////////////////////////////////////////////
    let body = {
        size: 1000,
        from: 0,
        "query": {
            "bool" : {

                "must" : {
                    "term" : { "categories.alias" : place } 
                 },

                 "filter" : {
                    "term" : { "location.address1" : where } 
                 },
     
                 "filter" : {
                    "term" : { "location.zip_code" : zipcode } 
                 },
     
     
                "must_not" : {
                "range" : {
                  "rating" : { "lte" : 3 }
                }
              },

              "must_not" : {
                "range" : {
                  "review_count" : { "lte" : 500 }
                }
              },

              "should" : [
                { "term" : { "is_closed" : "false" } }
              ],
            }
          }
      }


    results = await esClient.search({index: 'chicago_yelp_reviews', body: body});

    results.hits.hits.forEach((hit, index) => {
        

        var place = {
                "name": hit._source.name,
                "display_phone": hit._source.display_phone,
                "address1": hit._source.location.address1,
                "zip_code": hit._source.location.zip_code,
                "is_closed": hit._source.is_closed,
                "rating": hit._source.rating,
                "review_count": hit._source.review_count,
                "latitude": hit._source.coordinates.latitude,    
                "longitude": hit._source.coordinates.longitude
        };

        places_found.push(place);

    });

    find_places_task_completed = true;       
}

async function find_places_from_yelp_zipcode_only(zipcode) {

    places_found = [];

//////////////////////////////////////////////////////////////////////////////////////
// Using the business name to search for businesses will leead to incomplete results
// better to search using categorisa/alias and title associated with the business name
// For example one of the famous places in chicago for HotDogs is Portillos
// However, it also offers Salad and burgers
// Here is an example of a busness review from Yelp for Pertilos
//               alias': 'portillos-hot-dogs-chicago-4',
//              'categories': [{'alias': 'hotdog', 'title': 'Hot Dogs'},
//                             {'alias': 'salad', 'title': 'Salad'},
//                             {'alias': 'burgers', 'title': 'Burgers'}],
//              'name': "Portillo's Hot Dogs",
//////////////////////////////////////////////////////////////////////////////////////
    let body = {
        size: 1000,
        from: 0,
        "query": {
            "bool" : {
     
                 "must" : {
                    "term" : { "location.zip_code" : zipcode } 
                 },
     
     
                "must_not" : {
                "range" : {
                  "rating" : { "lte" : 3 }
                }
              },

              "must_not" : {
                "range" : {
                  "review_count" : { "lte" : 500 }
                }
              },

              "should" : [
                { "term" : { "is_closed" : "false" } }
              ],
            }
          }
      }


    results = await esClient.search({index: 'chicago_yelp_reviews', body: body});

    results.hits.hits.forEach((hit, index) => {
        

        var place = {
                "name": hit._source.name,
                "display_phone": hit._source.display_phone,
                "address1": hit._source.location.address1,
                "zip_code": hit._source.location.zip_code,
                "is_closed": hit._source.is_closed,
                "rating": hit._source.rating,
                "review_count": hit._source.review_count,
                "latitude": hit._source.coordinates.latitude,    
                "longitude": hit._source.coordinates.longitude
        };

        places_found.push(place);

    });

    find_places_task_completed = true;       
}

app.use('/', router);

app.listen(4000, () => console.log('Express server running on port 4000'));

