////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////


/// This file and the source code provided can be used only for   
/// the projects and assignments of this course

/// Last Edit by Dr. Atef Bader: 1/30/2019


////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////



import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router'; 
import { PlacesService } from '../../places.service';
import {FormControl,Validator} from '@angular/forms';


function ratingvalidator(control: FormControl)
{
  let rating= control.value;
 
}

@Component({
  selector: 'app-find  [ngModel]',
  templateUrl: './find.component.html',
  styleUrls: ['./find.component.css']
})
export class FindComponent implements OnInit {

  createForm: FormGroup;
  hintColor;
  rating;

 // ratingmsg = new FormControl('', [Validators.required, Validators.email]);

  constructor(private placesService: PlacesService, private fb: FormBuilder, private router: Router) {
    this.hintColor = "#76FF03";
    this.createForm = this.fb.group({
      where: ['',[Validators.minLength(2),Validators.maxLength(15) ]],
      find: ['',[Validators.minLength(2),Validators.maxLength(15) ]],
      zipcode:''
    });

    this.createForm.get('where').valueChanges.subscribe()
    {

    }
  }

/*   findPlaces(find, where) {
     
    this.placesService.findPlaces(find,where).subscribe(() => {

      this.router.navigate(['/list_of_places']);
    });


  } */

  findPlaces(find, where, zipcode) {
    
    if(zipcode != "" && find === "" && where === "") {
      this.placesService.findAllPlacesZip(zipcode).subscribe(() => { this.router.navigate(['/list_of_places']);});
    }
    else if(zipcode === "" && find != "" && where != "") {
      this.placesService.findPlaces(find, where).subscribe(() => { this.router.navigate(['/list_of_places']);});
    }
    else if(zipcode != "" && find != "" && where != "") {
      this.placesService.findPlacesZip(find, where, zipcode).subscribe(() => { this.router.navigate(['/list_of_places']);});
    }
    else {
      alert("Invalid conditions. Please try again!")
    }
  }  
  ngOnInit() {
  }

}
