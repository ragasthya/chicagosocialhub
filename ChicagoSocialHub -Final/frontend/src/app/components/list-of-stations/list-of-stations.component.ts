////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////


/// This file and the source code provided can be used only for   
/// the projects and assignments of this course

/// Last Edit by Dr. Atef Bader: 1/30/2019


////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////



import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material';

import { Station } from '../../station';
import { PlacesService } from '../../places.service';

import { Station_docks } from '../../station_docks';

import { Input, ViewChild, NgZone} from '@angular/core';
import { MapsAPILoader, AgmMap } from '@agm/core';
import { GoogleMapsAPIWrapper } from '@agm/core/services';
import { Place } from 'src/app/place';




interface Location {
  lat: number;
  lng: number;
  zoom: number;
  address_level_1?:string;
  address_level_2?: string;
  address_country?: string;
  address_zip?: string;
  address_state?: string;
  label: string;
}



@Component({
  selector: 'app-list-of-stations',
  templateUrl: './list-of-stations.component.html',
  styleUrls: ['./list-of-stations.component.css']
})
export class ListOfStationsComponent implements OnInit {

  stations: Station[];
  markers: Station[];
  placeSelected: Place;

  location:Location;

  station_docks: Station_docks[]=[];


  displayedColumns = ['id', 'stationName', 'availableBikes', 'availableDocks', 'is_renting', 'lastCommunicationTime', 'latitude',  'longitude', 'status', 'totalDocks', 'percent_filled', 'lineChart'];

  icon = {
    url: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
    scaledSize: {
      width: 60,
      height: 60
    }
  }


  constructor(private placesService: PlacesService, private router: Router, private http: HttpClient) { }

  

  ngOnInit() {
    this.fetchStations();
    this.getPlaceSelected();

    if (navigator)
    {
    navigator.geolocation.getCurrentPosition( pos => {
      this.location = {
        lat: pos.coords.latitude,
        lng: pos.coords.longitude,
        label: 'You are Here',
        zoom: 12
      };
      });
    }
  }

  fetchStations() {
    this.placesService
      .getStations()
      .subscribe((data: Station[]) => {
        this.stations = data;
        this.markers = data;
        console.log("INfo  "+data);
      });
  }

  getPlaceSelected() {
    this.placesService
      .getPlaceSelected()
      .subscribe((data: Place) => {
        this.placeSelected = data;

      });
  }
  



clickedMarker(label: string, index: number) {
  console.log(`clicked the marker: ${label || index}`)
}


circleRadius:number = 5000; // km
  
findDivyStations(id) {

  for (var i = 0,len = this.station_docks.length; i < len; i++) {

    if ( this.station_docks[i].id === id  ) { // strict equality test

        var station_selected =  this.station_docks[i];

        break;
    }
  }
  console.log("station found"+id);
  
  this.placesService.findDivyStations(id).subscribe(() => {
    this.router.navigate(['/line-chart']);
  });


  console.log("Line chart 2");
}


findDivyStationsSMA(id) {

  for (var i = 0,len = this.station_docks.length; i < len; i++) {

    if ( this.station_docks[i].id === id  ) { // strict equality test

        var station_selected =  this.station_docks[i];

        break;
    }
  }
  console.log("station found"+id);
  
  this.placesService.findDivyStationsSMA(id).subscribe(() => {
    this.router.navigate(['/line-chart-SMA']);
  });


  
  console.log("Line chart 2");
}


StackView() {

  
  this.placesService.getStations().subscribe(() => {
    this.router.navigate(['/stack-bar-chart']);
  });

  
  console.log("Line chart 2");
}




}





