import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import * as d3 from 'd3-selection';
import * as d3_obj from 'd3';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import * as timeParse from 'd3-time-format';
import * as time from 'd3-time';
import { formatDate } from "@angular/common";

import { Station_docks } from '../station_docks';
import { Router } from '@angular/router';
import { Station } from '../station';
import { Place } from 'src/app/place';
import { PlacesService } from '../places.service';
import {Observable} from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs';
import * as papa from 'papaparse';





@Component({
    selector: 'app-line-chart-hour',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './line-chart-hour.component.html',
    styleUrls: ['./line-chart-hour.component.css']
})
export class LineCharthourComponent implements OnInit {

    title = 'Line Chart for past 24 Hours';
    stations_docks: Station_docks[];
   markers: Station_docks[];

    private margin = {top: 20, right: 20, bottom: 30, left: 50};
    private width: number;
    private height: number;
    private x: any;
    private y: any;
    private svg: any;
    private line: d3Shape.Line<[number, number]>;
    private line2: d3Shape.Line<[number, number]>;
    

    constructor( private placesService: PlacesService, private router: Router) {
        this.width = 500 - this.margin.left - this.margin.right;
        this.height = 500 - this.margin.top - this.margin.bottom;
       // setInterval(()=> { this.fetchStationsdockshour() },125000);
        
    }

    ngOnInit() {

    this.fetchStationsdockshour();
    setInterval(()=> { this.fetchStationsdockshour() },9000);

        
    }
    
    fetchStationsdockshour() {

        var svg = d3.select("svg");
        svg.selectAll("*").remove();
     

        this.placesService
        .getStationsdocks()
        .subscribe((raw_data: Station_docks[]) => {
        this.stations_docks = raw_data;
        var data = raw_data.slice(1, 30);
        var mean_data = raw_data.slice(1, 31).reverse();
        var movingMean = [];        
          
        var parseDate = timeParse.timeFormat("%H:%M");


           this.svg = d3.select('#linehoursvg')
           .append('g')
           .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
           
           this.x = d3Scale.scaleTime().range([0, this.width]);
           this.y = d3Scale.scaleLinear().range([this.height, 0]);
           this.x.domain(d3Array.extent(data, (d) => new Date(d.lastCommunicationTime.toString()) )); 
           //this.y.domain(d3Array.extent(data, (d) => d.availableDocks ));
           this.y.domain([0, d3_obj.max(data, function(d) { return d.availableDocks;})]);

           
            this.svg.append('g')
           .attr('class', 'axis axis--x')
           .attr('transform', 'translate(0,' + this.height + ')')
         //  .call(d3Axis.axisBottom(this.x))
           //.ticks(10).tickFormat(timeParse.timeFormat("%H")));
           .call(d3Axis.axisBottom(this.x).ticks(10).tickFormat(timeParse.timeFormat("%H:%M")))
           .selectAll("text")	
               .style("text-anchor", "end")
                //.attr("dx", "-.8em")
                // .attr("dy", ".15em")
               .attr("transform", "rotate(-65)");

       this.svg.append('g')
           .attr('class', 'axis axis--y')
           .call(d3Axis.axisLeft(this.y))
           .append('text')
           .attr('class', 'axis-title')
           .attr('transform', 'rotate(-90)')
           .attr('y', 10)
           .attr('dy', '.71em')
           .style('text-anchor', 'end')
           .text('Available Docks');

           this.line = d3Shape.line()
           .x( (d: any) => this.x(new Date(d.lastCommunicationTime.toString())) )
           .y( (d: any) => this.y(d.availableDocks) );

           this.line2 = d3Shape.line()
           .x( (d: any) => this.x(new Date(d.x)) )
           .y( (d: any) => this.y(d.y) );


       this.svg.append('path')
           .datum(data)
           .attr('class', 'line')
           .attr('d', this.line);

           this.svg.append('path')
           .datum(movingMean)
           .attr('class', 'line2')
           .attr('d', this.line2);

             

        }) ;
       
    }

    fetchStationsdockshourSMA() {

        this.placesService
        .getStationsdocks()
        .subscribe((raw_data: Station_docks[]) => {
        this.stations_docks = raw_data;
        var data = raw_data.slice(1, 120);
        var mean_data = raw_data.slice(1, 120).reverse();
        var movingMean = [];        
        console.log(mean_data);
        var communcationTimes = [];
            movingMean.push({x: mean_data[mean_data.length - 1], y: 0});
        for (var i=mean_data.length - 2; i > 0; i--) {
            console.log("READING: " + i);
            var avaialble_docs_i = Number(mean_data[i].availableDocks);
            var avaialble_docs_i1 = Number(mean_data[i+1].availableDocks);
            var avaialble_docs_i2 = Number(mean_data[i-1].availableDocks);
            var avg = avaialble_docs_i + avaialble_docs_i1 + avaialble_docs_i2; 
            avg = avg / 3;
            movingMean.push({x:mean_data[i].lastCommunicationTime, y:avg})
        }
        movingMean = movingMean.reverse();
            
        console.log(data);
          
        var parseDate = timeParse.timeFormat("%H:%M");


           this.svg = d3.select('#linehoursvg')
           .append('g')
           .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
           
           this.x = d3Scale.scaleTime().range([0, this.width]);
           this.y = d3Scale.scaleLinear().range([this.height, 0]);
           this.x.domain(d3Array.extent(data, (d) => new Date(d.lastCommunicationTime.toString()) )); 
           //this.y.domain(d3Array.extent(data, (d) => d.availableDocks ));
           this.y.domain([0, d3_obj.max(data, function(d) { return d.availableDocks;})]);

           
            this.svg.append('g')
           .attr('class', 'axis axis--x')
           .attr('transform', 'translate(0,' + this.height + ')')
           .call(d3Axis.axisBottom(this.x).tickFormat(timeParse.timeFormat("%H:%M")));

       this.svg.append('g')
           .attr('class', 'axis axis--y')
           .call(d3Axis.axisLeft(this.y))
           .append('text')
           .attr('class', 'axis-title')
           .attr('transform', 'rotate(-90)')
           .attr('y', 6)
           .attr('dy', '.71em')
           .style('text-anchor', 'end')
           .text('Available Docks');

           this.line = d3Shape.line()
           .x( (d: any) => this.x(new Date(d.lastCommunicationTime.toString())) )
           .y( (d: any) => this.y(d.availableDocks) );

           this.line2 = d3Shape.line()
           .x( (d: any) => this.x(new Date(d.x)) )
           .y( (d: any) => this.y(d.y) );


       this.svg.append('path')
           .datum(data)
           .attr('class', 'line')
           .attr('d', this.line);

           this.svg.append('path')
           .datum(movingMean)
           .attr('class', 'line2')
           .attr('d', this.line2);

             

        }) ;
       
    }
    yourfunc2(e) {
        if(e.target.checked){  
            var svg = d3.select("svg");
            svg.selectAll("*").remove();
            this.fetchStationsdockshourSMA();  
            
        }
        else
        {
            var svg = d3.select("svg");
            svg.selectAll("*").remove();
            this.fetchStationsdockshour();
        }
     }
   
}

/*
<div>
		&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;
<input type="checkbox" id="Avg" class="regular-checkbox" (change)="yourfunc2($event)"/>
    <label>Simple Moving Average</label>
  </div>
*/