////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////


/// This file and the source code provided can be used only for   
/// the projects and assignments of this course

/// Last Edit by Dr. Atef Bader: 1/30/2019


////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////



import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { BarChartComponent } from './bar-chart/bar-chart.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule, GoogleMapsAPIWrapper } from '@agm/core';


import { MatToolbarModule, MatFormFieldModule, MatInputModule, MatOptionModule, MatSelectModule, MatIconModule, MatButtonModule, MatCardModule, MatTableModule, MatDividerModule, MatSnackBarModule } from '@angular/material';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { PlacesService } from './places.service';

import { FindComponent } from './components/find/find.component';
import { ListOfPlacesComponent } from './components/list-of-places/list-of-places.component';
import { ListOfStationsComponent } from './components/list-of-stations/list-of-stations.component';
import { StackBarChartComponent } from './stack-bar-chart/stack-bar-chart.component';
import { LineChartComponent } from './line-chart/line-chart.component';
import { MovingLineChart } from './moving-line-chart/moving-line-chart.component';
import { LineCharthourComponent } from './line-chart-hour/line-chart-hour.component';
import { LineCharthoursevendayComponent } from './line-chart-seven-day/line-chart-seven-day.component';

import {LineChartplacesComponent} from './line-chart-places/line-chart-places.component';
import {LineChartSMAComponent} from './line-chart-SMA/line-chart-SMA.component';
import {LineCharthourSMAComponent} from './line-chart-hour-SMA/line-chart-hour-SMA.component';
import {LineCharthoursevendaySMAComponent} from './line-chart-seven-day-SMA/line-chart-seven-day-SMA.component';
import {PieChartComponent} from './pie-chart/pie-chart.component';


const routes: Routes = [
  { path: 'find', component: FindComponent},
  { path: 'list_of_places', component: ListOfPlacesComponent},
  { path: 'list_of_stations', component: ListOfStationsComponent},
  { path: 'bar-chart', component: BarChartComponent },
  { path: 'stack-bar-chart', component: StackBarChartComponent},
  { path: 'line-chart', component: LineChartComponent},
  { path: 'moving-line-chart', component: MovingLineChart},
  { path: '', redirectTo: 'find', pathMatch: 'full'},
  { path: 'line-chart-hour', component: LineCharthourComponent},
  { path: 'line-chart-seven-day', component: LineCharthoursevendayComponent},
  {path:'line-chart-places',component: LineChartplacesComponent},
  {path:'line-chart-SMA',component: LineChartSMAComponent },
  {path:'line-chart-hour-SMA',component: LineCharthourSMAComponent },  
  {path:'line-chart-Seven-day-SMA',component: LineCharthoursevendaySMAComponent },  
  {path:'pie-chart',component: PieChartComponent },  
  

  
];

@NgModule({
  declarations: [
    AppComponent,
    FindComponent,
    BarChartComponent,
    ListOfPlacesComponent,
    ListOfStationsComponent,  
    StackBarChartComponent, 
    LineCharthourComponent,
    LineChartComponent,
    LineCharthoursevendayComponent,
    MovingLineChart,
    LineChartplacesComponent,
    LineChartSMAComponent,
    LineCharthourSMAComponent,
    LineCharthoursevendaySMAComponent,
    PieChartComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatTableModule,
    MatDividerModule,
    MatSnackBarModule,

/////////////////////////////////////////////////////////////////////////////////////    
/////////////////////////// SETUP NEEDED ////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

//  1. Create your API key from Google Developer Website
//  2. Install AGM package: npm install @agm/core @ng-bootstrap/ng-bootstrap --
//  3. Here is the URL for an online IDE for NG and TS that could be used to experiment
//  4. AGM live demo is loacted at this URL: https://stackblitz.com/edit/angular-google-maps-demo


/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////


    AgmCoreModule.forRoot({apiKey: 'AIzaSyDnJYexIVAvwqr7FdqSI3LPdGalw65Yg4A'}),
    FormsModule,
    NgbModule
  ],

  providers: [PlacesService, GoogleMapsAPIWrapper],
  bootstrap: [AppComponent]
})
export class AppModule { }
