import { Component, ViewEncapsulation, OnInit,Input } from '@angular/core';

import * as d3 from 'd3-selection';

import * as d3Scale from 'd3-scale';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import { STATISTICS } from '../shared';
import { FormControl, Validator } from '@angular/forms';




import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';


import { Place } from '../place';


import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material';


import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { PlacesService } from '../places.service';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };


@Component({
    selector: 'app-bar-chart',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './bar-chart.component.html',
    styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnInit {

  title1 = 'REVIEW COUNTS';
  title2 = 'RATINGS';
  
  uri = 'http://localhost:4000';

  places: Place[]=[];

    private width: number;
    private height: number;
    private margin = {top: 20, right: 20, bottom: 30, left: 40};

    private x: any;
    private y: any;
    private svg: any;
    private g: any;

    fetchPlaces() {
        this.placesService
          .getPlaces()
          .subscribe((data: Place[]) => {
            this.places = data;
          
           this.svg = d3.select('svg');
           this.width = 900 - this.margin.left - this.margin.right;
           this.height = 500 - this.margin.top - this.margin.bottom;
           this.g = this.svg.append('g')
               .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
      
 
               this.x = d3Scale.scaleBand().rangeRound([0, this.width]).padding(.751);
               this.y = d3Scale.scaleLinear().rangeRound([this.height, 0]);
               this.x.domain(this.places.map((d) => d.name));
               this.y.domain([0, d3Array.max(this.places, (d) => Number(d.review_count))]);
        
               this.g.append('g')
               .attr('class', 'axis axis--x')
               .attr('transform', 'translate(0,' + this.height + ')')
               .call(d3Axis.axisBottom(this.x))
               .selectAll("text")	
               .style("text-anchor", "end")
               .attr("transform", "rotate(-65)");
               

                this.g.append('g')
               .attr('class', 'axis axis--y')
               .call(d3Axis.axisLeft(this.y).ticks(10))
               .append('text')
               .attr('class', 'axis-title')
               .attr('transform', 'rotate(-90)')
               .attr('y', 10)
               .attr('dy', '0.71em')
               .attr('text-anchor', 'end')
               .text('Review Count');
               
               this.g.selectAll('.bar')
               .data(this.places)
               .enter().append('rect')
               .attr('class', 'bar')
               .attr('x', (d) => this.x(d.name) )
               .attr('y', (d) => this.y(Number(d.review_count)) )
               .attr('width', this.x.bandwidth())
               .attr('height', (d) => this.height - this.y(Number(d.review_count)) );
      


          });
  
      }
    
      fetchPlaces2() {
        this.placesService
          .getPlaces()
          .subscribe((data: Place[]) => {
            this.places = data;
          


           this.svg = d3.select('#svg2');
           this.width = 900 - this.margin.left - this.margin.right;
           this.height = 500 - this.margin.top - this.margin.bottom;
           this.g = this.svg.append('g')
               .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
      
 
               this.x = d3Scale.scaleBand().rangeRound([0, this.width]).padding(.751);
               this.y = d3Scale.scaleLinear().rangeRound([this.height, .099]);
               this.x.domain(this.places.map((d) => d.name));
               this.y.domain([0, d3Array.max(this.places, (d) => Number(d.rating))]);
          
               this.g.append('g')
               .attr('class', 'axis axis--x')
               .attr('transform', 'translate(0,' + this.height + ')')
               .call(d3Axis.axisBottom(this.x))
               .selectAll("text")	
               .style("text-anchor", "end")
               // .attr("dx", "-.8em")
                // .attr("dy", ".15em")
               .attr("transform", "rotate(-65)")


                this.g.append('g')
               .attr('class', 'axis axis--y')
               .call(d3Axis.axisLeft(this.y).ticks(10))
               .append('text')
               .attr('class', 'axis-title')
               .attr('transform', 'rotate(-90)')
               .attr('y', 10)
               .attr('dy', '0.71em')
               .attr('text-anchor', 'end')
               .text('Rating');
               
               this.g.selectAll('.bar')
               .data(this.places)
               .enter().append('rect')
               .attr('class', 'bar')
               .attr('x', (d) => this.x(d.name) )
               .attr('y', (d) => this.y(Number(d.rating)) )
               .attr('width', this.x.bandwidth())
               .attr('height', (d) => this.height - this.y(Number(d.rating)) );
      


          });
  
      }



    constructor(private placesService: PlacesService, private router: Router, private http: HttpClient) { 
        
    }
    
    ngOnInit() {
        this.fetchPlaces();
       this.fetchPlaces2();
        //this.initAxis();
        //this.drawAxis();
        //this.drawBars();
        
           }

    
}
