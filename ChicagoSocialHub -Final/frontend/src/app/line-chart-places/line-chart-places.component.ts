import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import * as d3 from 'd3-selection';
import * as d3_obj from 'd3';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import * as timeParse from 'd3-time-format';
import * as time from 'd3-time';
import { formatDate } from "@angular/common";
//import {timeParse} from "d3-time-format";
  

import { Station_docks } from '../station_docks';
import { Router } from '@angular/router';
import { Station } from '../station';
import { Place } from 'src/app/place';
import { PlacesService } from '../places.service';
import {Observable} from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs';
import * as papa from 'papaparse';





@Component({
    selector: 'app-line-chart-places',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './line-chart-places.component.html',
    styleUrls: ['./line-chart-places.component.css']
})
export class LineChartplacesComponent implements OnInit {

    title = 'REVIEW COUNTS';
    title2 = 'RATINGS';
    
    //stations_docks: Station_docks[];
   ///markers: Station_docks[];

   private line: d3Shape.Line<[number, number]>;

     
  uri = 'http://localhost:4000';

  places: Place[]=[];

    private width: number;
    private height: number;
    private margin = {top: 20, right: 20, bottom: 30, left: 40};

    private x: any;
    private y: any;
    private svg_line: any;
    private g: any;

    
    constructor( private placesService: PlacesService, private router: Router) {
        this.width = 900 - this.margin.left - this.margin.right;
        this.height = 500 - this.margin.top - this.margin.bottom;
      //  setInterval(()=> { this.linechartplaces() },125000);
        
    }

    ngOnInit() {

        this.linechartplaces();
        this.linechartplaces2();
    }
    
    linechartplaces() {

         var svg_line = d3.select("svg_line");
        svg_line.selectAll("*").remove();
        

        this.placesService
        .getPlaces()
        .subscribe((data: Place[]) => {
        this.places = data;
        
           // this.markers = data;
            
        console.log(data);
        
      //  var parseDate = timeParse.timeFormat("%d-%b-%y");

           this.svg_line = d3.select('#svg_line')
           .append('g')
           .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
           
           this.x = d3Scale.scaleBand().rangeRound([0, this.width]).padding(.91);
         //  this.x = d3Scale.scaleTime().range([0, this.width]);
           this.y = d3Scale.scaleLinear().range([this.height, 0]);
           this.x.domain(this.places.map((d) => d.name));
           //  this.x.domain(d3Array.extent(data, (d) => (d.name.toString()) )); 
           this.y.domain([0, d3Array.max(this.places, (d) => Number(d.review_count))]);
          
            this.svg_line.append('g')
            .attr('class', 'axis axis--x')
            .attr('transform', 'translate(0,' + this.height + ')')
            .call(d3Axis.axisBottom(this.x))
            .selectAll("text")	
            .style("text-anchor", "end")
            // .attr("dx", "-.8em")
            // .attr("dy", ".15em")
            .attr("transform", "rotate(-65)");
            //.attr("transform", "rotate(-65)");

            
            this.svg_line.append('g')
           .attr('class', 'axis axis--y')
           .call(d3Axis.axisLeft(this.y))
           .append('text')
           .attr('class', 'axis-title')
           .attr('transform', 'rotate(-90)')
           .attr('y', 6)
           .attr('dy', '.71em')
           .style('text-anchor', 'end')
           .text('Review Counts');

           this.line = d3Shape.line()
           .x( (d: any) => this.x((d.name)) )
           .y( (d: any) => this.y(d.review_count) );

       this.svg_line.append('path')
           .datum(data)
           .attr('class', 'line')
           .attr('d', this.line);
        }) ;
        
    }

    
    linechartplaces2() {

      var svg_line = d3.select("svg_line2");
     svg_line.selectAll("*").remove();
     

     this.placesService
     .getPlaces()
     .subscribe((data: Place[]) => {
     this.places = data;
     
        // this.markers = data;
         
     console.log(data);
     
   //  var parseDate = timeParse.timeFormat("%d-%b-%y");

        this.svg_line = d3.select('#svg_line2')
        .append('g')
        .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
        
        this.x = d3Scale.scaleBand().rangeRound([0, this.width]).padding(.91);
      //  this.x = d3Scale.scaleTime().range([0, this.width]);
        this.y = d3Scale.scaleLinear().range([this.height, 0]);
        this.x.domain(this.places.map((d) => d.name));
        //  this.x.domain(d3Array.extent(data, (d) => (d.name.toString()) )); 
        this.y.domain([0, d3Array.max(this.places, (d) => Number(d.rating))]);
       
         this.svg_line.append('g')
         .attr('class', 'axis axis--x')
         .attr('transform', 'translate(0,' + this.height + ')')
         .call(d3Axis.axisBottom(this.x))
         .selectAll("text")	
         .style("text-anchor","end")
         //.attr("dx", "-.8em")
         //.attr("dy", ".15em")
         .attr("transform", "rotate(-65)")
         //.attr("transform", "rotate(-65)");

         
         this.svg_line.append('g')
        .attr('class', 'axis axis--y')
        .call(d3Axis.axisLeft(this.y))
        .append('text')
        .attr('class', 'axis-title')
        .attr('transform', 'rotate(-90)')
        .attr('y', 6)
        .attr('dy', '.71em')
        .style('text-anchor', 'end')
        .text('Rating');

        this.line = d3Shape.line()
        .x( (d: any) => this.x((d.name)) )
        .y( (d: any) => this.y(d.rating) );

    this.svg_line.append('path')
        .datum(data)
        .attr('class', 'line')
        .attr('d', this.line);
     }) ;
     
 }
}
