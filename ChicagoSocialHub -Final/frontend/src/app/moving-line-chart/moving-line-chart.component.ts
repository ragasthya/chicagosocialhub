import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import { formatDate } from "@angular/common";

import { STOCKS } from '../shared';
import { Station_docks } from '../station_docks';
import { Router } from '@angular/router';
import { Station } from '../station';
import { Place } from 'src/app/place';
import { PlacesService } from '../places.service';
import {Observable} from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs';
import * as papa from 'papaparse';





@Component({
    selector: 'app-moving-line-chart',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './moving-line-chart.component.html',
    styleUrls: ['./moving-line-chart.component.css']
})
export class MovingLineChart implements OnInit {

    
    constructor( private placesService: PlacesService, private router: Router) {
        
        
    }

    ngOnInit() {


        

        
    }

}

