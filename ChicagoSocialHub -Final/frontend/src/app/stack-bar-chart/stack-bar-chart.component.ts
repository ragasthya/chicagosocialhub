import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Papa } from 'ngx-papaparse';
import * as d32 from 'd3';
import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import { Place } from 'src/app/place';
import { Router } from '@angular/router';
import { Station } from '../station';
import { PlacesService } from '../places.service';
import {Observable} from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { of } from 'rxjs';
import * as papa from 'papaparse';



import { SAMPLE_DATA } from '../shared/data04';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

export interface Margin {
    top: number;
    right: number;
    bottom: number;
    left: number;
}

@Component({
    selector: 'app-stack-bar-chart',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './stack-bar-chart.component.html',
    styleUrls: ['./stack-bar-chart.component.css']
})


export class StackBarChartComponent implements OnInit {
    
    title = 'Stacked Bar Chart';
    
    stations: Station[];
    markers: Station[];
    placeSelected: Place;
    
    private margin: Margin;

    private width: number;
    private height: number;

    private svg: any;     // TODO replace all `any` by the right type

    private x: any;
    private y: any;
    private z: any;
    private g: any;

    constructor( private placesService: PlacesService, private router: Router ) {}

    ngOnInit() {
        this.fetchStations2();
    }

    

    fetchStations2() {
        this.placesService
          .getStations()
          .subscribe((data: Station[]) => {
            this.stations = data;
            this.markers = data;
            console.log("Stack Bar Chart  "+data);
            
                this.margin = {top: 20, right: 20, bottom: 30, left: 40};
        
                this.svg = d3.select('svg');
        
                this.width = +this.svg.attr('width') - this.margin.left - this.margin.right;
                this.height = +this.svg.attr('height') - this.margin.top - this.margin.bottom;
                this.g = this.svg.append('g')
                .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')')
                .attr('class', 'axis axis--x');
        
                this.x = d3Scale.scaleBand()
                    .rangeRound([0, this.width])
                    .paddingInner(0.55)
                    .align(0.215);
                    

                this.y = d3Scale.scaleLinear()
                    .rangeRound([this.height, 0]);

                this.z = d3Scale.scaleOrdinal()
                    .range(['#ff2020', '#ddff00']);
        
                this.drawChart(data);
            }
            );         
      }


      private drawChart(data: any[]) {

        let keys = Object.getOwnPropertyNames(data[0]).slice(2,4);

        data = data.map(v => {
            v.total = keys.map(key => v[key]).reduce((a, b) => a + b, 0);
            return v;
        });
        data.sort((a: any, b: any) => b.total - a.total);

        this.x.domain(data.map((d: any) => d.stationName));
        this.y.domain([0, d3Array.max(data, (d: any) => d.total)]).nice();
        this.z.domain(keys);

        this.g.append('g')
            .selectAll('g')
            .data(d3Shape.stack().keys(keys)(data))
            .enter().append('g')
            .attr('fill', d => this.z(d.key))
            .selectAll('rect')
            .data(d => d)
            .enter().append('rect')
            .attr('x', d => this.x(d.data.stationName))
            .attr('y', d => this.y(d[1]))
            .attr('font-size', 25)
            .attr('height', d => this.y(d[0]) - this.y(d[1]))
            .attr('width', this.x.bandwidth());

        this.g.append('g')
            .attr('class', 'axis')
            .attr('transform', 'translate(0,' + this.height + ')')
            .call(d3Axis.axisBottom(this.x));

        this.g.append('g')
            .attr('class', 'axis')
            .call(d3Axis.axisLeft(this.y).ticks(null, 's'))
            .append('text')
            .attr('x', 2)
            .attr('y', this.y(this.y.ticks().pop()) + 0.9)
            .attr('dy', '0.32em')
            .attr('fill', '#000')
            .attr('font-weight', 'bold')
            .attr('text-anchor', 'start')
            .text('');

        let legend = this.g.append('g')
            .attr('font-family', 'sans-serif')
            .attr('font-size', 15)
            .attr('text-anchor', 'end')
            .selectAll('g')
            .data(keys.slice().reverse())
            .enter().append('g')
            .attr('transform', (d, i) => 'translate(0,' + i * 20 + ')');

        legend.append('rect')
            .attr('x', this.width - 19)
            .attr('font-size', 25)
            .attr('width', 19)
            .attr('height', 19)
            .attr('fill', this.z);

        legend.append('text')
            .attr('x', this.width - 24)
            .attr('y', 9.5)
            .attr('dy', '0.32em')
            .text(d => d);
    }

    
}
