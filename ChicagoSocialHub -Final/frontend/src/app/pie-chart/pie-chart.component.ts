import { Component, ViewEncapsulation, OnInit,Input } from '@angular/core';

import * as d3 from 'd3-selection';
import * as d3Shape from 'd3';
import * as d3Scale from 'd3-scale';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';
import { STATISTICS } from '../shared';




import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';


import { Place } from '../place';


import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material';


import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { PlacesService } from '../places.service';

@Component({
    selector: 'app-pie-chart',
    encapsulation: ViewEncapsulation.None,
    templateUrl: './pie-chart.component.html',
    styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {

    
  uri = 'http://localhost:4000';

  places: Place[]=[];

    title = 'Pie Chart';
    

    private margin = {top: 20, right: 20, bottom: 30, left: 50};
    private width: number;
    private height: number;
    private radius: number;

    private arc: any;
    private labelArc: any;
    private pie: any;
    private color: any;
    private svg: any;

    
    constructor(private placesService: PlacesService, private router: Router, private http: HttpClient) {
        this.width = 900 - this.margin.left - this.margin.right;
        this.height = 500 - this.margin.top - this.margin.bottom;
        this.radius = Math.min(this.width, this.height) / 2;
       

    }

    ngOnInit() {
      this.piechartplaces();
    }

     piechartplaces() {
  
        
}
}
